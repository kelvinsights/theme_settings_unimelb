<?

function THEME_preprocess_html(&$variables, $hook) {

  //External header injection from brand.unimelb.edu.au
  drupal_add_js("http://brand.unimelb.edu.au/global-header/js/injection.js", "external");

  //External css from brand.unimelb.edu.au
  drupal_add_css('http://brand.unimelb.edu.au/web-templates/1-1-0/css/complete.css', array('type' => 'external'));
  drupal_add_css('http://brand.unimelb.edu.au/global-header/css/style.css', array('type' => 'external'));


  /* Classes */
  //add logo
  $variables['classes_array'][] = "no-logo";

  // add home if is front page
  if(drupal_is_front_page()) $variables['classes_array'][] = "home"; else $variables['classes_array'][] = "blue";

  //??? not sure what is headingblock
  $variables['classes_array'][] = "headingblock";

}

function THEME_preprocess_block(&$variables, $hook) {
  if ($variables['block_html_id'] == 'block-delta-blocks-site-name'){
    $variables['classes_array'][] = "hgroup";
  }
}
